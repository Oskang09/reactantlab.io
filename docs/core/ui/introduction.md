---
title: Introduction to UI
sidebar_label: Introduction to UI
---

## About Reactant UI
Reactant UI is an element based UI system, you can build your inventory GUI with the elements, 
reactant will render the inventory with those elements. 

If you have some basic knowledge about web development, 
you will find that it is so similar to the html & css concepts.

### What is the difference between element based UI and other common UI libraries?

When comparing to the old school UI libraries, 
those libraries only provide a so far convenient way for you 'paint' your UI, 
you still have to manage the state of the painted items. 

Let's use pseudocode to have an example, now we want to make a 9x6 inventory UI, which have:
 - A background fill with glass pane
 - An apple 3x3 rectangle in the top left corner

To do this with the old school UI:
```
fillRectangle item=GLASS_PANE size=9,6
fillRectangle item=APPLE size=3,3
```

What if we now want to move the apple rectangle 2 unit to the right?

```
fillRectangle item=GLASS_PANE size=9,6 
fillRectangle item=APPLE size=3,3 position=2,0
```

Although it looks dumb, but this is what actually we have to do with the 'paint' UI libraries. 
What if there have more objects? Fish rectangle? Beef items? Cookies? 
It is definitely not a good idea to manage the state by yourself, 
and it is absolutely not fun to calculate all the objects position and handle the overlapping problem.

Let's see how reactant handles the UI problem.

For the first problem:
```
div {
    fill=GLASS_PANE
    size=9,6

    div {
        id="appleRectangle"
        fill=APPLE
        size=3,3
    }
}
```

Hm... It looks like no big difference, or even more worse? Okay, let's look on the next problem.

```
ui.getElementById("appleRectangle").position=2,0
```

Yes, this is how reactant works. You are not required to care about how to render the inventory, 
you just need to tell reactant what you want to render, reactant will help you solve the problem then.
